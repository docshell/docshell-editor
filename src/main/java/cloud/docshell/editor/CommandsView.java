package cloud.docshell.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.azserve.azframework.vaadin.flow.component.AZEditableGrid;
import com.azserve.azframework.vaadin.flow.component.AZGrid;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import cloud.docshell.schema.docshell.Command;
import cloud.docshell.schema.docshell.Comment;
import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.KeyValue;

public class CommandsView extends VerticalLayout {

	private static final long serialVersionUID = 2130375709674904645L;

	private final HorizontalLayout splitLayout = new HorizontalLayout();
	private final AZGrid<Command> commandTable = new AZGrid<>(Command.class, false);
	private final VerticalLayout commandDetail = new VerticalLayout();

	private final TextField commandId = new TextField("Id");
	private final TextArea comment = new TextArea("Comment");
	private final Checkbox ignore = new Checkbox("Ignore");
	private final TextField shellType = new TextField("Shell type");
	private final AZEditableGrid<KeyValue> keyValueTable = new AZEditableGrid<>(KeyValue.class, true);
	private final Binder<Command> commandBinder = new Binder<>(Command.class);
	private final VerticalLayout ioDetail = new VerticalLayout();

	public CommandsView() {
		super();
		this.add(this.splitLayout);
		this.splitLayout.setSizeFull();
		this.splitLayout.add(this.commandTable);
		this.commandTable.setWidth("300px");
		this.splitLayout.add(this.commandDetail);
		this.commandDetail.add(this.commandId);
		this.commandDetail.add(this.comment);
		this.commandDetail.add(this.ignore);
		this.commandDetail.add(this.shellType);
		this.commandDetail.add(this.keyValueTable);

		this.commandBinder.bind(this.commandId, Command::getId, Command::setId);
		this.commandBinder.bind(this.comment,
				d -> Optional.ofNullable(d.getComment()).map(o -> o.getValue()).orElse(""),
				(d, v) -> {
					if (v == null || v.trim().equals("")) {
						d.setComment(null);
					} else {
						Comment currentComment = d.getComment();
						if (currentComment == null) {
							currentComment = new Comment();
							d.setComment(currentComment);
						}
						currentComment.setValue(v);
					}
				});
		this.commandBinder.bind(this.ignore, d -> d.isIgnore(), (d, v) -> d.setIgnore(v));
		this.commandBinder.bind(this.shellType, d -> d.getShellType(), (d, v) -> d.setShellType(v));

		this.splitLayout.add(this.ioDetail);

		this.ioDetail.setWidth("1000px");
		this.initCommandTable();
		this.initKeyValueTable();
	}

	private void initCommandTable() {
		this.commandTable.addColumn(p -> p.getId()).setHeader("Id");
		this.commandTable.addColumn(p -> p.getShellType()).setHeader("Shell");
		this.commandTable.addSelectionListener(e -> {
			final Optional<Command> selectedItem = e.getFirstSelectedItem();
			this.commandBinder.setBean(selectedItem.orElse(null));
			if (selectedItem.isPresent()) {
				this.keyValueTable.setItems(new ArrayList<>(selectedItem.get().getProperty()));
				this.fillIoTable(selectedItem.get());
			} else {
				this.keyValueTable.setItems(new ArrayList<>());
				this.fillIoTable(null);
			}
		});
	}

	private void initKeyValueTable() {
		this.keyValueTable.setInsertCallback(() -> {
			final KeyValue keyValue = new KeyValue();
			final List<KeyValue> property = this.commandTable.getSelectedItems().iterator().next().getProperty();
			property.add(keyValue);
			this.keyValueTable.getItems().clear();
			this.keyValueTable.getItems().addAll(new ArrayList<>(property));
			return keyValue;
		});
		this.keyValueTable.setDeleteCallback(e -> {
			final List<KeyValue> property = this.commandTable.getSelectedItems().iterator().next().getProperty();
			property.remove(e);
			this.keyValueTable.getItems().clear();
			this.keyValueTable.getItems().addAll(new ArrayList<>(property));
			return true;
		});
		final Binder<KeyValue> keyValueBinder = this.keyValueTable.getBinder();
		final TextField key = new TextField();
		keyValueBinder.bind(key, KeyValue::getKey, KeyValue::setKey);
		this.keyValueTable.addColumn(p -> p.getKey()).setEditorComponent(key).setHeader("Key");
		final TextField value = new TextField();
		keyValueBinder.bind(value, KeyValue::getValue, KeyValue::setValue);
		this.keyValueTable.addColumn(p -> p.getValue()).setEditorComponent(value).setHeader("Value");
		this.keyValueTable.addEditorColumns();
	}

	private void fillIoTable(final Command command) {
		this.ioDetail.removeAll();

		if (command == null) {
			return;
		}

		final List<Io> ios = command.getIo();
		for (final Io io : ios) {
			this.ioDetail.add(new IoPanel(io));
		}

		// this.ioTable.setInsertCallback(() -> {
		// final Io keyValue = new Io();
		// final List<Io> property = this.commandTable.getSelectedItems().iterator().next().getIo();
		// property.add(keyValue);
		// this.ioTable.getItems().clear();
		// this.ioTable.getItems().addAll(new ArrayList<>(property));
		// return keyValue;
		// });
		// this.ioTable.setDeleteCallback(e -> {
		// final List<Io> property = this.commandTable.getSelectedItems().iterator().next().getIo();
		// property.remove(e);
		// this.ioTable.getItems().clear();
		// this.ioTable.getItems().addAll(new ArrayList<>(property));
		// return true;
		// });
		//
		// final Binder<Io> ioBinder = this.ioTable.getBinder();
		// final TextArea value = new TextArea();
		// ioBinder.bind(value, Io::getValue, Io::setValue);
		// this.ioTable.addColumn(p -> p.getValue()).setEditorComponent(value).setHeader("Value");
		//
		// final Select<IoType> type = new Select<>(IoType.values());
		// ioBinder.bind(type, Io::getType, Io::setType);
		// this.ioTable.addColumn(p -> p.getType()).setEditorComponent(type).setHeader("Type");
		// this.ioTable.addEditorColumns();
	}

	public void setDocShell(final Docshell docshell) {
		this.commandTable.setItems(new ArrayList<>(docshell.getCommand()));
	}

	public void toDocshell(final Docshell docshell) {
		final Collection<Command> items = this.commandTable.getItems();
		docshell.getCommand().clear();
		docshell.getCommand().addAll(items);
	}
}

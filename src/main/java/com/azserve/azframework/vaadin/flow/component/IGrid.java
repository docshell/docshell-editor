package com.azserve.azframework.vaadin.flow.component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import com.sun.istack.Nullable;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.data.binder.PropertySet;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.ValueProvider;

public interface IGrid<T> {

	Column<T> addColumn(String propertyName);

	<V extends Component> Column<T> addComponentColumn(ValueProvider<T, V> componentProvider);

	default <E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>, V> Column<T> addFieldColumn(
			final ValueProvider<T, C> fieldProvider,
			final SerializableFunction<T, V> getter,
			final Setter<T, V> setter) {
		return this.addFieldColumn(fieldProvider, getter, setter, (a, b) -> {});
	}

	default <E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>, V> Column<T> addFieldColumn(
			final ValueProvider<T, C> fieldProvider,
			final SerializableFunction<T, V> getter,
			final Setter<T, V> setter,
			final boolean refreshItemOnClientChange) {

		final BiConsumer<T, E> valueChangedCallback = refreshItemOnClientChange
				? (item, event) -> {
					if (event.isFromClient()) {
						this.getDataProvider().refreshItem(item);
					}
				}
				: (a, b) -> {};
		return this.addFieldColumn(fieldProvider, getter, setter, valueChangedCallback);
	}

	default <E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>, V> Column<T> addFieldColumn(
			final ValueProvider<T, C> fieldProvider,
			final SerializableFunction<T, V> getter,
			final Setter<T, V> setter,
			final BiConsumer<T, E> valueChangedCallback) {

		return this.addComponentColumn(dto -> {
			final C field = fieldProvider.apply(dto);
			if (field != null) {
				field.setValue(getter.apply(dto));
				field.addValueChangeListener(event -> {
					final V value = event.getValue();
					setter.accept(dto, value);
					valueChangedCallback.accept(dto, event);
				});
			}
			return field;
		});
	}

	// selection

	/**
	 * Selects all items available if the selection model
	 * permits multi selection, otherwise does nothing.
	 */
	default void selectAll() {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (selectionModel instanceof GridMultiSelectionModel) {
			((GridMultiSelectionModel<T>) selectionModel).selectAll();
		}
	}

	/**
	 * Selects the given item causing other items
	 * to be deselected.
	 *
	 * @param item the item to be selected.
	 */
	default void setSelectedItem(final @Nullable T item) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (item == null) {
			selectionModel.deselectAll();
		} else {
			this.setSelectedItems(Collections.singleton(item));
		}
	}

	/**
	 * Selects the given items causing other items
	 * to be deselected.
	 *
	 * @param items the items to be selected.
	 * @throws IllegalArgumentException if the grid selection model
	 *             is not a {@code GridMultiSelectionModel} instance and
	 *             multiple items are about to be selected.
	 */
	default void setSelectedItems(final Collection<T> items) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (items.isEmpty()) {
			selectionModel.deselectAll();
			return;
		}
		if (selectionModel instanceof GridMultiSelectionModel) {
			final GridMultiSelectionModel<T> multiSelectionModel = (GridMultiSelectionModel<T>) selectionModel;
			multiSelectionModel.updateSelection(new HashSet<>(items), multiSelectionModel.getSelectedItems()
					.stream().filter(item -> !items.contains(item)).collect(Collectors.toCollection(HashSet::new)));
		} else if (items.size() > 1) {
			throw new IllegalArgumentException("Selection model must be a GridMultiSelectionModel instance to select multiple items");
		}
		selectionModel.select(items.iterator().next());
	}

	// accessors

	GridSelectionModel<T> getSelectionModel();

	DataProvider<T, ?> getDataProvider();

	PropertySet<T> getPropertySet();

	List<GridSortOrder<T>> getSortOrder();

	// utility

	static <T> Column<T> fixDefaultsForColumn(final Column<T> column, final ValueProvider<T, ?> valueProvider) {
		final SerializableComparator<T> comparator = column.getComparator(SortDirection.ASCENDING);
		column.setComparator((o1, o2) -> {
			final Object v1 = valueProvider.apply(o1);
			final Object v2 = valueProvider.apply(o2);
			return v1 instanceof String || v2 instanceof String
					? String.CASE_INSENSITIVE_ORDER.compare(Objects.toString(v1, ""), Objects.toString(v2, ""))
					: comparator.compare(o1, o2);
		});
		return column;
	}
}

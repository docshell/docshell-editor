package cloud.docshell.editor;

import static java.util.stream.Collectors.toList;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;

import com.azserve.azframework.vaadin.flow.component.AZEditableGrid;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;

import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.Param;

public class HeadView extends FormLayout {

	private static final long serialVersionUID = 4532891891513172259L;

	private final TextField title = new TextField("title");
	private final TextField author = new TextField("author");
	private final TextField id = new TextField("id");
	private final DateTimePicker date = new DateTimePicker("date");
	private final TextField version = new TextField("version");
	private final TextField osFamily = new TextField("os.family");
	private final TextField osDistribution = new TextField("os.distribution");
	private final TextField osVersion = new TextField("os.version");

	private final AZEditableGrid<Param> params = new AZEditableGrid<>(Param.class, false);
	private final AZEditableGrid<Tag> tags = new AZEditableGrid<>(Tag.class, false);

	private final Binder<Docshell> binder = new Binder<>(Docshell.class);

	public HeadView() {
		this.add(this.title);
		this.add(this.author);
		this.add(this.id);
		this.add(this.date);
		this.add(this.version);
		this.add(this.osFamily);
		this.add(this.osDistribution);
		this.add(this.osVersion);
		this.add(this.params);
		this.add(this.tags);

		this.binder.bind(this.title, Docshell::getTitle, Docshell::setTitle);
		this.binder.bind(this.author, Docshell::getAuthor, Docshell::setAuthor);
		this.binder.bind(this.id, Docshell::getId, Docshell::setId);
		this.binder.bind(this.date, d -> d.getDate().toLocalDateTime(), (d, c) -> d.setDate(c.atOffset(ZoneOffset.UTC)));
		this.binder.bind(this.version, Docshell::getVersion, Docshell::setVersion);
		this.binder.bind(this.osFamily, d -> d.getOs().getFamily(), (d, c) -> d.getOs().setFamily(c));
		this.binder.bind(this.osDistribution, d -> d.getOs().getDistribution(), (d, c) -> d.getOs().setDistribution(c));
		this.binder.bind(this.osVersion, d -> d.getOs().getVersion(), (d, c) -> d.getOs().setVersion(c));

		this.params.setInsertCallback(Param::new);
		final Binder<Param> paramBinder = this.params.getBinder();
		final TextField paramName = new TextField();
		final TextField paramDescription = new TextField();
		final TextField paramDefaultValue = new TextField();
		paramBinder.bind(paramName, Param::getName, Param::setName);
		paramBinder.bind(paramDescription, Param::getDescription, Param::setDescription);
		paramBinder.bind(paramDefaultValue, Param::getDefaultValue, Param::setDefaultValue);
		this.params.addColumn(p -> p.getName()).setEditorComponent(paramName).setHeader("Name");
		this.params.addColumn(p -> p.getDescription()).setEditorComponent(paramDescription).setHeader("Description");
		this.params.addColumn(p -> p.getDefaultValue()).setEditorComponent(paramDefaultValue).setHeader("Default value");
		this.params.addEditorColumns();

		this.tags.setInsertCallback(Tag::new);
		final Binder<Tag> tagBinder = this.tags.getBinder();
		final TextField tag = new TextField();
		tagBinder.bind(tag, Tag::getTag, Tag::setTag);
		this.tags.addColumn(Tag::getTag).setEditorComponent(tag).setHeader("Tag");
		this.tags.addEditorColumns();

	}

	public void setDocShell(final Docshell docshell) {
		this.binder.readBean(docshell);
		this.params.setItems(new ArrayList<>(docshell.getParam()));
		this.tags.setItems(docshell.getTag().stream().map(Tag::new));
	}

	public void toDocshell(final Docshell docshell) {
		try {
			this.binder.writeBean(docshell);

			final Collection<Param> items = this.params.getItems();
			docshell.getParam().clear();
			docshell.getParam().addAll(items);

			final Collection<String> tagList = this.tags.getItems().stream().map(Tag::getTag).filter(s -> s.trim().length() > 0).collect(toList());
			docshell.getTag().clear();
			docshell.getTag().addAll(tagList);
		} catch (final ValidationException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	private static class Tag {

		String tag = "";

		public Tag() {}

		public Tag(final String tag) {
			this.tag = tag;
		}

		public String getTag() {
			return this.tag;
		}

		public void setTag(final String tag) {
			this.tag = tag;
		}

	}

}

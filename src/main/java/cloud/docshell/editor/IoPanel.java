package cloud.docshell.editor;

import java.util.UUID;
import java.util.function.Consumer;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.PendingJavaScriptResult;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;

import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.IoType;

public class IoPanel extends VerticalLayout {

	private static final long serialVersionUID = 3222636478257216586L;
	private final TextArea valueField = new TextArea();
	private final Button save = new Button(VaadinIcon.CHECK.create());
	private final Button delete = new Button(VaadinIcon.TRASH.create());
	private final Button split = new Button(VaadinIcon.SCISSORS.create());
	private final Button preview = new Button(VaadinIcon.PRESENTATION.create());
	private final Select<IoType> type = new Select<>(IoType.values());
	private final Io io;
	private final UUID uuid = UUID.randomUUID();

	public IoPanel(final Io io) {
		this.io = io;

		final HorizontalLayout buttonsBar = new HorizontalLayout(this.save, this.delete, this.split, this.preview);
		final HorizontalLayout left = new HorizontalLayout(this.type, buttonsBar);

		this.add(left);
		this.add(this.valueField);
		this.valueField.setWidthFull();
		this.valueField.setId(this.uuid.toString());

		this.type.setValue(io.getType());
		this.valueField.setValue(io.getValue());

		this.addOnSplit(null);
	}

	public void addOnDelete(final Consumer<Io> callback) {
		this.delete.addClickListener(e -> callback.accept(this.io));
	}

	public void addOnSplit(final Consumer<Io> callback) {
		this.split.addClickListener(e -> {
			final PendingJavaScriptResult executeJs = this.valueField.getElement().executeJs("alert(document.getElementById(\"" + this.uuid + "\").childNodes.length);" + "document.getElementById(\"" + this.uuid + "\").select();");
			executeJs.then(Object.class, System.out::println);

		});
	}

}

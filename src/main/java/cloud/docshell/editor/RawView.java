package cloud.docshell.editor;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;

public class RawView extends VerticalLayout {

	private static final long serialVersionUID = 1119181868329568187L;

	private final TextArea raw = new TextArea();

	public RawView() {
		// TODO: load from file/url
		this.raw.setSizeFull();
		// TODO monospace font
		this.add(this.raw);
	}

	public String getRaw() {
		return this.raw.getValue();
	}

	public void setRaw(final String raw) {
		this.raw.setValue(raw == null ? "" : raw);
	}

}

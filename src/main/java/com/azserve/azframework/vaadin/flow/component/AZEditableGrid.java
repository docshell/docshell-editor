package com.azserve.azframework.vaadin.flow.component;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;

@SuppressWarnings("unchecked")
public class AZEditableGrid<T> extends AZGrid<T> {

	private static final long serialVersionUID = 5510873783687064777L;

	private static final String COL_EDIT = "edit";
	private static final String COL_DELETE = "delete";

	private final Binder<T> binder;

	private InsertCallback<T> insertCallback;
	private Predicate<T> editablePredicate = a -> true;
	private Predicate<T> deletablePredicate = a -> true;
	private Consumer<T> saveCallback = a -> {};
	private DeleteCallback<T> deleteCallback = item -> true;

	private Button btnInsert;

	public AZEditableGrid(final Class<T> itemClass, final boolean autoCommit) {
		this.binder = new Binder<>(itemClass);
		this.getEditor().setBinder(this.binder);
		this.getEditor().setBuffered(!autoCommit);
		this.getEditor().addSaveListener(e -> this.saveCallback.accept(e.getItem()));
		this.getElement().addEventListener("keyup", e -> this.cancel())
				.setFilter("event.key === 'Escape' || event.key === 'Esc'");
	}

	public Binder<T> getBinder() {
		return this.binder;
	}

	@Override
	public void setDataProvider(final DataProvider<T, ?> dataProvider) {
		this.cancel();
		super.setDataProvider(dataProvider);
	}

	/**
	 * Creates and adds an edit and a delete column.
	 *
	 * @throws IllegalStateException if the columns have already been added.
	 */
	public void addEditorColumns() {

		if (this.btnInsert != null) {
			throw new IllegalStateException("Editor columns already created");
		}
		final Button _btnInsert = this.createButton(Action.INSERT);
		_btnInsert.setVisible(this.insertCallback != null);
		_btnInsert.addClickListener(e -> this.insert());

		final Button btnSave = this.createButton(Action.SAVE);
		btnSave.addClickListener(e -> this.save());

		final Button btnCancel = this.createButton(Action.CANCEL);
		btnCancel.addClickListener(e -> this.cancel());

		this.addComponentColumn(item -> {
			if (this.getEditablePredicate().test(item)) {
				final Button btnEdit = this.createButton(Action.EDIT);
				btnEdit.addClickListener(e -> this.edit(item));
				return btnEdit;
			}
			return new Span();
		}).setEditorComponent(btnCancel).setFlexGrow(0).setWidth("5em").setKey(COL_EDIT);

		this.addComponentColumn(item -> {
			if (this.getDeletablePredicate().test(item)) {
				final Button btnDelete = this.createButton(Action.DELETE);
				btnDelete.addClickListener(e -> {
					if (this.deleteCallback.onDelete(item)) {
						this.removeItems(item);
					}
				});
				return btnDelete;
			}
			return new Span();
		}).setEditorComponent(btnSave).setFlexGrow(0).setWidth("5em").setKey(COL_DELETE).setHeader(_btnInsert);

		this.btnInsert = _btnInsert;
	}

	public Column<T> getEditColumn() {
		return this.getColumnByKey(COL_EDIT);
	}

	public Column<T> getInsertDeleteColumn() {
		return this.getColumnByKey(COL_DELETE);
	}

	public Predicate<T> getEditablePredicate() {
		return this.editablePredicate;
	}

	public void setEditablePredicate(final Predicate<T> editablePredicate) {
		this.editablePredicate = Objects.requireNonNull(editablePredicate);
	}

	public Predicate<T> getDeletablePredicate() {
		return this.deletablePredicate;
	}

	public void setDeletablePredicate(final Predicate<T> deletablePredicate) {
		this.deletablePredicate = Objects.requireNonNull(deletablePredicate);
	}

	public InsertCallback<T> getInsertCallback() {
		return this.insertCallback;
	}

	public void setInsertCallback(final InsertCallback<T> itemSupplier) {
		this.insertCallback = itemSupplier;
		if (this.btnInsert != null) {
			this.btnInsert.setVisible(itemSupplier != null);
		}
	}

	public Consumer<T> getSaveCallback() {
		return this.saveCallback;
	}

	public void setSaveCallback(final Consumer<T> saveCallback) {
		this.saveCallback = Objects.requireNonNull(saveCallback, "Save button callback cannot be null");
	}

	public DeleteCallback<T> getDeleteCallback() {
		return this.deleteCallback;
	}

	public void setDeleteCallback(final DeleteCallback<T> deleteCallback) {
		this.deleteCallback = Objects.requireNonNull(deleteCallback, "Delete button callback cannot be null");
	}

	public void insert() {
		if (this.insertCallback == null) {
			return;
		}
		final T item = this.insertCallback.onInsert();
		if (item == null) {
			return;
		}
		this.cancel();
		this.addItems(item);
		this.edit(item);
	}

	public void edit(final T item) {
		final Editor<T> editor = this.getEditor();
		if (editor.getItem() != null) {
			this.cancel();
		}
		editor.editItem(item);
	}

	public void delete(final T item) {
		this.cancel();
		this.removeItems(item);
	}

	protected void save() {
		this.getEditor().save();
	}

	public void cancel() {
		final Editor<T> editor = this.getEditor();
		final T item = editor.getItem();
		if (item != null) {
			editor.cancel();
			// if (this.isNewItem(item)) {
			// this.removeItems(item);
			// }
		}
	}

	protected Button createButton(final Action action) {
		final Button btn = new Button();
		// btn.setThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);
		switch (action) {
			case CANCEL:
				btn.setIcon(VaadinIcon.CLOSE.create());
				break;
			case DELETE:
				btn.setIcon(VaadinIcon.TRASH.create());
				btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
				break;
			case EDIT:
				btn.setIcon(VaadinIcon.PENCIL.create());
				break;
			case INSERT:
				btn.setIcon(VaadinIcon.PLUS.create());
				btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				break;
			case SAVE:
				btn.setIcon(VaadinIcon.CHECK.create());
				btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);
				break;
		}
		return btn;
	}

	public interface InsertCallback<T> {

		/**
		 * Executes an action when the insert
		 * button is clicked.
		 * <p>
		 * Can return a new item to add to the grid.
		 * If not, an item can be added manually using the
		 * {@link AZGrid#addItems(Object...)} method.
		 *
		 * @return a new item to add to the grid or {@code null}.
		 */
		T onInsert();
	}

	public interface DeleteCallback<T> {

		/**
		 * Executes an action when the delete
		 * button is clicked.
		 * <p>
		 * Should return whether or not remove the
		 * item from the grid. If not, the item
		 * can be removed manually using the
		 * {@link AZGrid#removeItems(Object...)} method.
		 *
		 * @param item the item to delete.
		 * @return {@code true} if the item has to be
		 *         removed from the grid, {@code false} to
		 *         do nothing.
		 */
		boolean onDelete(T item);
	}

	public enum Action {
		INSERT,
		EDIT,
		DELETE,
		SAVE,
		CANCEL;
	}
}

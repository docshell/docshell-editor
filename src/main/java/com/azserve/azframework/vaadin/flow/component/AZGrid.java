
package com.azserve.azframework.vaadin.flow.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.function.ValueProvider;

/**
 * Enhanced grid using an underlying {@code ArrayList} by default.
 * 
 * @author filippo.ortolan
 * @since 29/11/2018
 * @param <T> the bean type.
 */
public class AZGrid<T> extends Grid<T> {

	private static final long serialVersionUID = 1170625422672011338L;

	public AZGrid() {
		super();
		this.initialize();
	}

	public AZGrid(final int pageSize) {
		super(pageSize);
		this.initialize();
	}

	public AZGrid(final Class<T> beanType, final boolean autoCreateColumns) {
		super(beanType, autoCreateColumns);
		this.initialize();
	}

	public AZGrid(final Class<T> beanType) {
		super(beanType);
		this.initialize();
	}

	private void initialize() {
		this.setItems(new ArrayList<>());
	}

	@Override
	public Column<T> addColumn(final ValueProvider<T, ?> valueProvider) {
		return IGrid.fixDefaultsForColumn(super.addColumn(valueProvider), valueProvider);
	}

	@Override
	public <V extends Comparable<? super V>> Column<T> addColumn(final ValueProvider<T, V> valueProvider, final String... sortingProperties) {
		return IGrid.fixDefaultsForColumn(super.addColumn(valueProvider, sortingProperties), valueProvider);
	}

	/** * Adds the specified items to the underlying collection * and refreshes the grid. * * @param items items to add. * @throws IllegalStateException if the data provider is not an instance of {@code ListDataProvider}. * @throws UnsupportedOperationException if the underlying collection is unmodifiable. */
	public void addItems(final @SuppressWarnings("unchecked") T... items) {
		final ListDataProvider<T> listDataProvider = this.getListDataProvider().orElseThrow(() -> new IllegalStateException("Cannot add items to a non ListDataProvider instance"));
		Collections.addAll(listDataProvider.getItems(), items);
		listDataProvider.refreshAll();
	}

	/** * Removes the specified items from the underlying collection * and refreshes the grid. * * @param items items to remove. * @throws IllegalStateException if the data provider is not an instance of {@code ListDataProvider}. * @throws UnsupportedOperationException if the underlying collection is unmodifiable. */
	public void removeItems(final @SuppressWarnings("unchecked") T... items) {
		final ListDataProvider<T> listDataProvider = this.getListDataProvider().orElseThrow(() -> new IllegalStateException("Cannot remove items from a non ListDataProvider instance"));
		for (final T item : items) {
			listDataProvider.getItems().remove(item);
		}
		listDataProvider.refreshAll();
	}

	/** * Gets the underlying collection used to show data in grid. * * @throws IllegalStateException if the data provider is not an instance of {@code ListDataProvider}. */
	public Collection<T> getItems() {
		return this.getListDataProvider().map(ListDataProvider::getItems).orElseThrow(() -> new IllegalStateException("Cannot get items from a non ListDataProvider instance"));
	}

	/** * Gets a ordered copy of the underlying collection based on * the user sort order. * * @throws IllegalStateException if the data provider is not an instance of {@code ListDataProvider}. */
	public List<T> getSortedItems() {
		final Collection<T> items = this.getListDataProvider().map(ListDataProvider::getItems).orElseThrow(() -> new IllegalStateException("Cannot sort items from a non ListDataProvider instance"));
		final List<GridSortOrder<T>> sortOrders = this.getSortOrder();
		final Iterator<GridSortOrder<T>> iterator = sortOrders.iterator();
		if (iterator.hasNext()) {
			GridSortOrder<T> sortOrder = iterator.next();
			Comparator<T> comparator = sortOrder.getSorted().getComparator(sortOrder.getDirection());
			while (iterator.hasNext()) {
				sortOrder = iterator.next();
				comparator = comparator.thenComparing(sortOrder.getSorted().getComparator(sortOrder.getDirection()));
			}
			return items.stream().sorted(comparator).collect(Collectors.toList());
		}
		return new ArrayList<>(items);
	}

	@SuppressWarnings("unchecked")
	private Optional<ListDataProvider<T>> getListDataProvider() {
		final DataProvider<T, ?> dataProvider = this.getDataProvider();
		if (dataProvider instanceof ListDataProvider) {
			return Optional.of((ListDataProvider<T>) dataProvider);
		}
		return Optional.empty();
	}
}

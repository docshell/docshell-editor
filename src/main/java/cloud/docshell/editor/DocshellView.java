package cloud.docshell.editor;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.SelectedChangeEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;

import cloud.docshell.schema.docshell.Docshell;

@Route("")
@PWA(name = "Docshell editor", shortName = "Docshell", enableInstallPrompt = false)
@Theme(value = Material.class)
public class DocshellView extends VerticalLayout {

	private static final long serialVersionUID = 5561864209691372801L;

	private final VerticalLayout tabContent = new VerticalLayout();
	private final Tab tabRaw = new Tab("Raw");
	private final Tab tabHead = new Tab("Head");
	private final Tab tabCommands = new Tab("Commands");
	private final Tab tabPreview = new Tab("Preview");

	private final RawView rawView = new RawView();
	private final HeadView headView = new HeadView();
	private final CommandsView commandsView = new CommandsView();

	private String xml;
	private Docshell docshell;

	public DocshellView() {
		this.tabContent.setSizeFull();
		final Tabs tabs = new Tabs();
		tabs.add(this.tabRaw, this.tabHead, this.tabCommands, this.tabPreview);
		tabs.addSelectedChangeListener(this::selectTab);
		this.add(tabs, this.tabContent);
		this.initTabRaw();
		this.setSizeFull();
		this.rawView.setSizeFull();
	}

	private void selectTab(final SelectedChangeEvent e) {
		this.tabContent.removeAll();
		final Tab previousTab = e.getPreviousTab();
		if (previousTab == this.tabRaw) {
			this.xml = this.rawView.getRaw();
			this.docshell = this.toDocshell();
			if (this.docshell == null) {
				e.getSource().setSelectedTab(this.tabRaw);
				return;
			}
		} else if (previousTab == this.tabHead) {
			this.headView.toDocshell(this.docshell);
		} else if (previousTab == this.tabCommands) {
			this.commandsView.toDocshell(this.docshell);
		} else if (previousTab == this.tabPreview) {

		}
		final Tab t = e.getSelectedTab();
		if (t == this.tabRaw) {
			this.initTabRaw();
		} else if (t == this.tabHead) {
			this.initTabHead();
		} else if (t == this.tabCommands) {
			this.initTabCommands();
		} else if (t == this.tabPreview) {

		}
	}

	private void initTabRaw() {
		this.xml = this.toXml();
		this.rawView.setRaw(this.xml);
		this.tabContent.add(this.rawView);
	}

	private void initTabHead() {
		this.headView.setDocShell(this.docshell);
		this.tabContent.add(this.headView);
	}

	private void initTabCommands() {
		this.commandsView.setDocShell(this.docshell);
		this.tabContent.add(this.commandsView);
	}

	private Docshell toDocshell() {
		try {
			final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			final Docshell ds = (Docshell) unmarshaller.unmarshal(new StringReader(this.xml));
			return ds;
		} catch (final Exception ex) {

			ex.printStackTrace();
			return null;
		}
	}

	private String toXml() {
		if (this.docshell == null) {
			return "";
		}
		try {
			final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			final StringWriter writer = new StringWriter();
			marshaller.marshal(this.docshell, writer);
			return writer.toString();
		} catch (final Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
